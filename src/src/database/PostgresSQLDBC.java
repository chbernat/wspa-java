package src.database;

import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import src.classes.User;
import src.classes.Post;
import src.classes.Comment;


public class PostgresSQLDBC {
	private final String url = "jdbc:postgresql://localhost:5432/postgres";
	private final String user = "postgres";
	private final String password = "postgres";
	
	public Connection connect() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}
	

//	public User getUserByLogin(String login) {
//		String SQL = "SELECT login FROM users";
//		
//	}
	
//	this.id = id;
//	this.body = body;
//	this.userId = userId;
//	this.postId = postId;
//	this.orderInPost = orderInPost;
	
	public int createUser(User user) {
		String SQL = "INSERT INTO \"users\" (id,username) VALUES(?,?)";
		int id = 0;
		try(Connection conn = connect();
	        PreparedStatement pstmt = conn.prepareStatement(SQL,
	                       				Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setInt(1, 0);
			pstmt.setString(2, user.getUsername());
			System.out.println(pstmt.toString());
			int affectedRows = pstmt.executeUpdate();
			if(affectedRows > 0) {
				try(ResultSet rs = pstmt.getGeneratedKeys()) {
					if(rs.next()){
						id = rs.getInt(1);
						return id;
					}
				} catch (SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());			
		}
		return id;
	}

	public long createPost(Post post) {
		String SQL = "INSERT INTO \"posts\" (title,body,userId,likes) VALUES(?,?,?,?)";
		long id = 0;
		try(Connection conn = connect();
	        PreparedStatement pstmt = conn.prepareStatement(SQL,
	                       				Statement.RETURN_GENERATED_KEYS)) {
//			pstmt.setInt(1, post.getId());
			pstmt.setString(1, post.getTitle());
			pstmt.setString(2, post.getBody());
			pstmt.setInt(3, post.getUserId());
			pstmt.setInt(4, post.getLikes());
			int affectedRows = pstmt.executeUpdate();
			if(affectedRows > 0) {
				try(ResultSet rs = pstmt.getGeneratedKeys()) {
					if(rs.next()){
						id = rs.getLong(1);
					}
				} catch (SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());			
		}
		return id;
	}

	public long createComment(Comment comment) {
		String SQL = "INSERT INTO \"comments\" (id,body,userId,postId,orderInPost) VALUES(?,?,?,?,?)";
		long id = 0;
		try(Connection conn = connect();
	        PreparedStatement pstmt = conn.prepareStatement(SQL,
	                       				Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setInt(1, comment.getId());
			pstmt.setString(2, comment.getBody());
			pstmt.setInt(3, comment.getUserId());
			pstmt.setInt(4, comment.getPostId());
			pstmt.setInt(5, comment.getOrderInPost());
			int affectedRows = pstmt.executeUpdate();
			if(affectedRows > 0) {
				try(ResultSet rs = pstmt.getGeneratedKeys()) {
					if(rs.next()){
						id = rs.getLong(1);
					}
				} catch (SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());			
		}
		return id;
	}
	
	public User getUser(int id) {
		String SQL = "SELECT * from \"users\" WHERE id=" + id;
		try(Connection conn = connect();
		    PreparedStatement pstmt = conn.prepareStatement(SQL)) {
	
				pstmt.executeQuery();
				try(ResultSet rs = pstmt.getResultSet()) {
					if(rs.next()){
						return new User(rs.getInt(1),
										rs.getString(1));
					}
				} catch (SQLException ex){
					System.out.println(ex.getMessage());
				}
				
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());			
			}
		return null;
	}

	public Post getPost(int id){
		String SQL = "SELECT * from \"posts\" WHERE id=" + id;
		try(Connection conn = connect();
		    PreparedStatement pstmt = conn.prepareStatement(SQL)) {
	
				pstmt.executeQuery();
				try(ResultSet rs = pstmt.getResultSet()) {
					if(rs.next()){
						return new Post(rs.getInt(1),
										rs.getString(2),
										rs.getString(3),
										rs.getInt(4),
										rs.getInt(5));
					}
				} catch (SQLException ex){
					System.out.println(ex.getMessage());
				}
				
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());			
			}
		return null;
	}

	public Comment getComment(int id){
		String SQL = "SELECT * from \"comments\" WHERE id=" + id;
		try(Connection conn = connect();
		    PreparedStatement pstmt = conn.prepareStatement(SQL)) {
	
				pstmt.executeQuery();
				try(ResultSet rs = pstmt.getResultSet()) {
					if(rs.next()){
						return new Comment(rs.getInt(1),
										rs.getString(2),
										rs.getInt(3),
										rs.getInt(4),
										rs.getInt(5));
					}
				} catch (SQLException ex){
					System.out.println(ex.getMessage());
				}
				
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());			
			}
		return null;
	}
	   public static void main( String args[] ) {
		   User user = new User(0, "stefan");
		   PostgresSQLDBC pgSql = new PostgresSQLDBC();
		   pgSql.getUser(0);
//		   pgSql.createUser(user);
	   }
}
