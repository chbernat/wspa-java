package src.classes;

public class Post {
	private int id;
	private int userId;
	private String title;
	private String body;
	private int likes;
	
	public Post(int id, String title, String body, int likes, int userId) {
		this.id = id;
		this.userId = userId;
		this.likes = likes;
		this.title = title;
		this.body = body;
	}
	
	public int getId(){
		return this.id;
	}
	public int getUserId(){
		return this.userId;
	}
	public int getLikes(){
		return this.likes;
	}
	public String getTitle(){
		return this.title;
	}
	public String getBody(){
		return this.body;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void incrementLikes() {
		this.likes++;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public void setBody(String body){
		this.body = body;
	}
	
}
