package src.classes;

public class Comment {
	private String body;
	private int orderInPost;
	private int postId;
	private int userId;
	private int id;
	
	public Comment(int id, String body, int orderInPost, int postId, int userId) {
		this.id = id;
		this.body = body;
		this.userId = userId;
		this.postId = postId;
		this.orderInPost = orderInPost;
		
	}
	public int getId() {
		return this.id;
	}
	public int getOrderInPost() {
		return this.orderInPost;
	}
	public int getUserId() {
		return this.userId;
	}
	public int getPostId(){
		return this.postId;
	}
	public String getBody(){
		return this.body;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setOrderInPost(int orderInPost) {
		this.orderInPost = orderInPost;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public void setBody(String body) {
		this.body = body;
	}
}
