package src.misc;

import src.classes.User;
import src.misc.UserSingleton;
import src.database.PostgresSQLDBC;

public class UserManager {
	private String username;
	public UserManager(String username){
		this.username = username;
	}
	
	public void createUser(){
		User user = new User(0, this.username);
		PostgresSQLDBC driver = new PostgresSQLDBC();
		UserSingleton userSingleton = UserSingleton.getInstance();
		userSingleton.setId(driver.createUser(user));
	}
}
