package src.misc;

public class UserSingleton {
	private static UserSingleton userSingletonInstance;
	private int id;
	private UserSingleton(){
	}
	
	public static UserSingleton getInstance() {
		if(userSingletonInstance == null){
			userSingletonInstance = new UserSingleton();
		}
		return userSingletonInstance;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

}
