package jFrames.posts;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import src.misc.PostManager;

public class JCreate extends JFrame {

	private JPanel contentPane;
	private JTextField postTitle;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JCreate frame = new JCreate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JCreate() {
		PostManager postManager = new PostManager();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextPane txtpnCreatePost = new JTextPane();
		txtpnCreatePost.setEditable(false);
		txtpnCreatePost.setText("Create post");
		txtpnCreatePost.setBounds(12, 0, 426, 21);
		contentPane.add(txtpnCreatePost);
		
		JTextArea postBody = new JTextArea();
		postBody.setBounds(73, 160, 365, 74);
		contentPane.add(postBody);
		
		postTitle = new JTextField();
		postTitle.setBounds(12, 73, 426, 32);
		contentPane.add(postTitle);
		postTitle.setColumns(10);
		
		
		JTextPane txtpnTitle = new JTextPane();
		txtpnTitle.setEditable(false);
		txtpnTitle.setText("Title");
		txtpnTitle.setBounds(12, 44, 73, 21);
		contentPane.add(txtpnTitle);
		
		JTextPane txtpnPostBody = new JTextPane();
		txtpnPostBody.setEditable(false);
		txtpnPostBody.setText("Post body");
		txtpnPostBody.setBounds(12, 129, 73, 21);
		contentPane.add(txtpnPostBody);
		
		JButton submitBtn = new JButton("Submit");
		submitBtn.setBounds(152, 246, 117, 25);
		submitBtn.addActionListener(new ActionListener() {
		       public void actionPerformed(ActionEvent ae){
		    	   postManager.createPost(0, postTitle.getText(), postBody.getText(), 0, 0);
		       }
		      });
		contentPane.add(submitBtn);
	}
}
